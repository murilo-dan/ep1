#ifndef URNA_HPP
#define URNA_HPP

#include <bits/stdc++.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include "candidatos.hpp"
#include "eleitor.hpp"

class Urna {
protected:
  vector<Candidatos> lista_de_candidatos;
  vector<Eleitor> lista_de_eleitores;
public:
  Urna(vector<Candidatos> lista_de_candidatos, vector<Eleitor> lista_de_eleitores);
  ~Urna();
  void urna(vector<Candidatos>&, vector<Eleitor>&);
};

#endif
