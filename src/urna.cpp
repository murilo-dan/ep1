#include "../inc/urna.hpp"


Urna::Urna(vector<Candidatos> lista_de_candidatos, vector<Eleitor> lista_de_eleitores){
  Urna::urna(lista_de_candidatos, lista_de_eleitores);
}

Urna::~Urna(){}


void Urna::urna(vector<Candidatos>& newlista_de_candidatos, vector<Eleitor>& newlista_de_eleitores){
  ifstream leitura_presidente("data/consulta_cand_2018_BR.csv");

  string ue, cargo, numero, candidato, sigla, partido;

  while(leitura_presidente.good()){
      getline(leitura_presidente, ue, ',');
      getline(leitura_presidente, cargo, ',');
      getline(leitura_presidente, numero, ',');
      getline(leitura_presidente, candidato, ',');
      getline(leitura_presidente, sigla, ',');
      getline(leitura_presidente, partido);

      Candidatos newCandidatos(ue, cargo, numero, candidato, sigla, partido);
      newlista_de_candidatos.push_back(newCandidatos);
    }
  newlista_de_candidatos.pop_back();

  ifstream leitura_outros("data/consulta_cand_2018_DF.csv");
  while(leitura_outros.good()){
      getline(leitura_outros, ue, ',');
      getline(leitura_outros, cargo, ',');
      getline(leitura_outros, numero, ',');
      getline(leitura_outros, candidato, ',');
      getline(leitura_outros, sigla, ',');
      getline(leitura_outros, partido);

      Candidatos newCandidatos(ue, cargo, numero, candidato, sigla, partido);
      newlista_de_candidatos.push_back(newCandidatos);
    }
  newlista_de_candidatos.pop_back();

  int presidente_vencedor[100] = {}, governador_vencedor[100] = {}, senador_1_vencedor[1000] = {}, senador_2_vencedor[1000] = {}, deputado_federal_vencedor[10000] = {}, deputado_distrital_vencedor[100000] = {};
  string presidente, governador, senador_1, senador_2, deputado_federal, deputado_distrital, decisao, nome, sn1;
  int i, aux, a, nr_eleitores, size_candidatos = newlista_de_candidatos.size(), votos_branco = 0, votos_nulo = 0;

  cout << "Digite o número de eleitores: ";
  cin >> nr_eleitores;
  system("clear");

  for(aux = 0; aux < nr_eleitores; aux++){
    cout << "Digite seu nome: ";
    cin >> nome;
    system("clear");

    while(1){
      cout << "Votar para deputado federal." << endl << "Digite BRANCO para votar em branco." << endl << "Digite 0 para votar nulo." << endl << "Número do candidato: ";
      cin >> deputado_federal;
      system("clear");
      if(deputado_federal == "BRANCO"){
        cout << "Voto em branco." << endl << "Digite CONFIRMA para votar em branco." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA"){
          votos_branco++;
          break;
        }
        else if(decisao == "CORRIGE") continue;
      }
      for(i = 0; i<size_candidatos; i++){
        if(deputado_federal == newlista_de_candidatos[i].getNR_CANDIDATO() && newlista_de_candidatos[i].getDS_CARGO() == "DEPUTADO FEDERAL"){
          cout << "Deputado federal" << endl;
          cout << "Número: " << newlista_de_candidatos[i].getNR_CANDIDATO() << endl;
          cout << "Nome: " << newlista_de_candidatos[i].getNM_URNA_CANDIDATO() << endl;
          cout << "Partido: " << newlista_de_candidatos[i].getNM_PARTIDO() << "(" << newlista_de_candidatos[i].getSG_PARTIDO() << ")" << endl << endl;
          break;
        }
      }
      if(i==size_candidatos){
        cout << "Voto nulo." << endl << "Digite CONFIRMA para votar nulo." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA"){
          deputado_federal = "NULO";
          votos_nulo++;
          break;
        }
        else if(decisao == "CORRIGE") continue;
        else cout << "Comando inválido. Retornando à tela de voto." << endl << endl;
        continue;
      }
      cout << "Digite CONFIRMA para votar neste candidato." << endl << "Digite CORRIGE para votar em outro candidato." << endl;
      cin >> decisao;
      system("clear");
      if(decisao == "CONFIRMA"){
        deputado_federal_vencedor[stoi(deputado_federal)]++;
        deputado_federal = newlista_de_candidatos[i].getNM_URNA_CANDIDATO() + " - " + newlista_de_candidatos[i].getNR_CANDIDATO();
        break;
      }
      else if(decisao == "CORRIGE") continue;
    }

    while(1){
      cout << "Votar para deputado distrital." << endl << "Digite BRANCO para votar em branco." << endl << "Digite 0 para votar nulo." << endl << "Número do candidato: ";
      cin >> deputado_distrital;
      system("clear");
      if(deputado_distrital == "BRANCO"){
        cout << "Voto em branco." << endl << "Digite CONFIRMA para votar em branco." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA"){
          votos_branco++;
          break;
        }
        else if(decisao == "CORRIGE") continue;
      }
      for(i = 0; i<size_candidatos; i++){
        if(deputado_distrital == newlista_de_candidatos[i].getNR_CANDIDATO() && newlista_de_candidatos[i].getDS_CARGO() == "DEPUTADO DISTRITAL"){
          cout << "Deputado distrital" << endl;
          cout << "Número: " << newlista_de_candidatos[i].getNR_CANDIDATO() << endl;
          cout << "Nome: " << newlista_de_candidatos[i].getNM_URNA_CANDIDATO() << endl;
          cout << "Partido: " << newlista_de_candidatos[i].getNM_PARTIDO() << "(" << newlista_de_candidatos[i].getSG_PARTIDO() << ")" << endl << endl;
          break;
        }
      }
      if(i==size_candidatos){
        cout << "Voto nulo." << endl << "Digite CONFIRMA para votar nulo." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA"){
          deputado_distrital = "NULO";
          votos_nulo++;
          break;
        }
        else if(decisao == "CORRIGE") continue;
        else cout << "Comando inválido. Retornando à tela de voto." << endl << endl;
        continue;
      }
      cout << "Digite CONFIRMA para votar neste candidato." << endl << "Digite CORRIGE para votar em outro candidato." << endl;
      cin >> decisao;
      system("clear");
      if(decisao == "CONFIRMA"){
        deputado_distrital_vencedor[stoi(deputado_distrital)]++;
        deputado_distrital = newlista_de_candidatos[i].getNM_URNA_CANDIDATO() + " - " + newlista_de_candidatos[i].getNR_CANDIDATO();
        break;
      }
      else if(decisao == "CORRIGE") continue;
    }

    while(1){
      cout << "Votar para senador (1º vaga)." << endl << "Digite BRANCO para votar em branco." << endl << "Digite 0 para votar nulo." << endl << "Número do candidato: ";
      cin >> senador_1;
      sn1 = senador_1;
      system("clear");
      if(senador_1 == "BRANCO"){
        cout << "Voto em branco." << endl << "Digite CONFIRMA para votar em branco." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA"){
          votos_branco++;
          break;
        }
        else if(decisao == "CORRIGE") continue;
      }
      for(i = 0; i<size_candidatos; i++){
        if(senador_1 == newlista_de_candidatos[i].getNR_CANDIDATO() && newlista_de_candidatos[i].getDS_CARGO() == "SENADOR"){
          cout << "Senador - 1º vaga" << endl;
          cout << "Número: " << newlista_de_candidatos[i].getNR_CANDIDATO() << endl;
          cout << "Nome: " << newlista_de_candidatos[i].getNM_URNA_CANDIDATO() << endl;
          cout << "Partido: " << newlista_de_candidatos[i].getNM_PARTIDO() << "(" << newlista_de_candidatos[i].getSG_PARTIDO() << ")" << endl;
          for(a = 0; a<size_candidatos; a++){
            if(senador_1 == newlista_de_candidatos[a].getNR_CANDIDATO() && newlista_de_candidatos[a].getDS_CARGO() == "1º SUPLENTE"){
              cout << "1º Suplente: " << newlista_de_candidatos[a].getNM_URNA_CANDIDATO() << endl;
            }
          }
          for(a = 0; a<size_candidatos; a++){
            if(senador_1 == newlista_de_candidatos[a].getNR_CANDIDATO() && newlista_de_candidatos[a].getDS_CARGO() == "2º SUPLENTE"){
              cout << "2º Suplente: " << newlista_de_candidatos[a].getNM_URNA_CANDIDATO() << endl << endl;
            }
          }
          break;
        }
      }
      if(i==size_candidatos){
        cout << "Voto nulo." << endl << "Digite CONFIRMA para votar nulo." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA"){
          senador_1 = "NULO";
          votos_nulo++;
          break;
        }
        else if(decisao == "CORRIGE") continue;
        else cout << "Comando inválido. Retornando à tela de voto." << endl << endl;
        continue;
      }
      cout << "Digite CONFIRMA para votar neste candidato." << endl << "Digite CORRIGE para votar em outro candidato." << endl;
      cin >> decisao;
      system("clear");
      if(decisao == "CONFIRMA"){
        senador_1_vencedor[stoi(senador_1)]++;
        senador_1 = newlista_de_candidatos[i].getNM_URNA_CANDIDATO() + " - " + newlista_de_candidatos[i].getNR_CANDIDATO();
        break;
      }
      else if(decisao == "CORRIGE") continue;
    }

    while(1){
      cout << "Votar para senador (2º vaga)." << endl << "Digite BRANCO para votar em branco." << endl << "Digite 0 para votar nulo." << endl << "Número do candidato: ";
      cin >> senador_2;
      system("clear");
      if(sn1 == senador_2){
        cout << "Voto nulo." << endl << "Digite CONFIRMA para votar nulo." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA"){
          votos_nulo++;
          senador_2 = "NULO";
          break;
        }
        else if(decisao == "CORRIGE") continue;
        else cout << "Comando inválido. Retornando à tela de voto." << endl << endl;
        continue;
      }
      if(senador_2 == "BRANCO"){
        cout << "Voto em branco." << endl << "Digite CONFIRMA para votar em branco." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA"){
          votos_branco++;
          break;
        }
        else if(decisao == "CORRIGE") continue;
      }
      for(i = 0; i<size_candidatos; i++){
        if(senador_2 == newlista_de_candidatos[i].getNR_CANDIDATO() && newlista_de_candidatos[i].getDS_CARGO() == "SENADOR"){
          cout << "Senador - 2º vaga" << endl;
          cout << "Número: " << newlista_de_candidatos[i].getNR_CANDIDATO() << endl;
          cout << "Nome: " << newlista_de_candidatos[i].getNM_URNA_CANDIDATO() << endl;
          cout << "Partido: " << newlista_de_candidatos[i].getNM_PARTIDO() << "(" << newlista_de_candidatos[i].getSG_PARTIDO() << ")" << endl;
          for(a = 0; a<size_candidatos; a++){
            if(senador_2 == newlista_de_candidatos[a].getNR_CANDIDATO() && newlista_de_candidatos[a].getDS_CARGO() == "1º SUPLENTE"){
              cout << "1º Suplente: " << newlista_de_candidatos[a].getNM_URNA_CANDIDATO() << endl;
            }
          }
          for(a = 0; a<size_candidatos; a++){
            if(senador_2 == newlista_de_candidatos[a].getNR_CANDIDATO() && newlista_de_candidatos[a].getDS_CARGO() == "2º SUPLENTE"){
              cout << "2º Suplente: " << newlista_de_candidatos[a].getNM_URNA_CANDIDATO() << endl << endl;
            }
          }
          break;
        }
      }
      if(i==size_candidatos){
        cout << "Voto nulo." << endl << "Digite CONFIRMA para votar nulo." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA"){
          senador_2 = "NULO";
          votos_nulo++;
          break;
        }
        else if(decisao == "CORRIGE") continue;
        else cout << "Comando inválido. Retornando à tela de voto." << endl << endl;
        continue;
      }
      cout << "Digite CONFIRMA para votar neste candidato." << endl << "Digite CORRIGE para votar em outro candidato." << endl;
      cin >> decisao;
      system("clear");
      if(decisao == "CONFIRMA"){
        senador_2_vencedor[stoi(senador_2)]++;
        senador_2 = newlista_de_candidatos[i].getNM_URNA_CANDIDATO() + " - " + newlista_de_candidatos[i].getNR_CANDIDATO();
        break;
      }
      else if(decisao == "CORRIGE") continue;
    }

    while(1){
      cout << "Votar para governador." << endl << "Digite BRANCO para votar em branco." << endl << "Digite 0 para votar nulo." << endl << "Número do candidato: ";
      cin >> governador;
      system("clear");
      if(governador == "BRANCO"){
        cout << "Voto em branco." << endl << "Digite CONFIRMA para votar em branco." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA"){
          votos_branco++;
          break;
        }
        else if(decisao == "CORRIGE") continue;
      }
      for(i = 0; i<size_candidatos; i++){
        if(governador == newlista_de_candidatos[i].getNR_CANDIDATO() && newlista_de_candidatos[i].getDS_CARGO() == "GOVERNADOR"){
          cout << "Governador" << endl;
          cout << "Número: " << newlista_de_candidatos[i].getNR_CANDIDATO() << endl;
          cout << "Nome: " << newlista_de_candidatos[i].getNM_URNA_CANDIDATO() << endl;
          cout << "Partido: " << newlista_de_candidatos[i].getNM_PARTIDO() << "(" << newlista_de_candidatos[i].getSG_PARTIDO() << ")" << endl;
          for(a = 0; a<size_candidatos; a++){
            if(governador == newlista_de_candidatos[a].getNR_CANDIDATO() && newlista_de_candidatos[a].getDS_CARGO() == "VICE-GOVERNADOR"){
              cout << "Vice-Governador: " << newlista_de_candidatos[a].getNM_URNA_CANDIDATO() << endl << endl;
            }
          }
          break;
        }
      }
      if(i==size_candidatos){
        cout << "Voto nulo." << endl << "Digite CONFIRMA para votar nulo." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA"){
          governador = "NULO";
          votos_nulo++;
          break;
        }
        else if(decisao == "CORRIGE") continue;
        else cout << "Comando inválido. Retornando à tela de voto." << endl << endl;
        continue;
      }
      cout << "Digite CONFIRMA para votar neste candidato." << endl << "Digite CORRIGE para votar em outro candidato." << endl;
      cin >> decisao;
      system("clear");
      if(decisao == "CONFIRMA"){
        governador_vencedor[stoi(governador)]++;
        governador = newlista_de_candidatos[i].getNM_URNA_CANDIDATO() + " - " + newlista_de_candidatos[i].getNR_CANDIDATO();
        break;
      }
      else if(decisao == "CORRIGE") continue;
    }

    while(1){
      cout << "Votar para presidente." << endl << "Digite BRANCO para votar em branco." << endl << "Digite 0 para votar nulo." << endl << "Número do candidato: ";
      cin >> presidente;
      system("clear");
      if(presidente == "BRANCO"){
        cout << "Voto em branco." << endl << "Digite CONFIRMA para votar em branco." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA"){
          votos_branco++;
          break;
        }
        else if(decisao == "CORRIGE") continue;
      }
      for(i = 0; i<size_candidatos; i++){
        if(presidente == newlista_de_candidatos[i].getNR_CANDIDATO() && newlista_de_candidatos[i].getDS_CARGO() == "PRESIDENTE"){
          cout << "Presidente" << endl;
          cout << "Número: " << newlista_de_candidatos[i].getNR_CANDIDATO() << endl;
          cout << "Nome: " << newlista_de_candidatos[i].getNM_URNA_CANDIDATO() << endl;
          cout << "Partido: " << newlista_de_candidatos[i].getNM_PARTIDO() << "(" << newlista_de_candidatos[i].getSG_PARTIDO() << ")" << endl;
          for(a = 0; a<size_candidatos; a++){
            if(presidente == newlista_de_candidatos[a].getNR_CANDIDATO() && newlista_de_candidatos[a].getDS_CARGO() == "VICE-PRESIDENTE"){
              cout << "Vice-presidente: " << newlista_de_candidatos[a].getNM_URNA_CANDIDATO() << endl << endl;
              break;
            }
          }
          break;
        }
      }
      if(i==size_candidatos){
        cout << "Voto nulo." << endl << "Digite CONFIRMA para votar nulo." << endl << "Digite CORRIGE para voltar à tela de voto." << endl;
        cin >> decisao;
        system("clear");
        if(decisao == "CONFIRMA"){
          presidente = "NULO";
          votos_nulo++;
          break;
        }
        else if(decisao == "CORRIGE") continue;
        else cout << "Comando inválido. Retornando à tela de voto." << endl << endl;
        continue;
      }
      cout << "Digite CONFIRMA para votar neste candidato." << endl << "Digite CORRIGE para votar em outro candidato." << endl;
      cin >> decisao;
      system("clear");
      if(decisao == "CONFIRMA"){
        presidente_vencedor[stoi(presidente)]++;
        presidente = newlista_de_candidatos[i].getNM_URNA_CANDIDATO() + " - " + newlista_de_candidatos[i].getNR_CANDIDATO();
        break;
      }
      else if(decisao == "CORRIGE") continue;
    }

    Eleitor newEleitor(nome, deputado_federal, deputado_distrital, senador_1, senador_2, governador, presidente);
    newlista_de_eleitores.push_back(newEleitor);
  }

  int M = 0;
  bool empate = false;

  unsigned int size = newlista_de_eleitores.size();
  cout << "Relatório de votação" << endl << endl;
  for(unsigned int i = 0; i < size; i++){
    cout << "Nome: " << newlista_de_eleitores[i].getNOME_ELEITOR() << endl;
    cout << "Voto para deputado federal: " << newlista_de_eleitores[i].getVOTO_DEPUTADO_FEDERAL() << endl;
    cout << "Voto para deputado distrital: " << newlista_de_eleitores[i].getVOTO_DEPUTADO_DISTRITAL() << endl;
    cout << "Voto para senador(1º vaga): " << newlista_de_eleitores[i].getVOTO_SENADOR_1() << endl;
    cout << "Voto para senador(2º vaga): " << newlista_de_eleitores[i].getVOTO_SENADOR_2() << endl;
    cout << "Voto para governador: " << newlista_de_eleitores[i].getVOTO_GOVERNADOR() << endl;
    cout << "Voto para presidente: " << newlista_de_eleitores[i].getVOTO_PRESIDENTE() << endl;
    cout << endl;
  }
  cout << "Resultado das eleições" << endl << endl;

  cout << "Presidente:" << endl << endl;
  for(i = 0, M = 0; i < 100; i++) {
    if(presidente_vencedor[i] == M && presidente_vencedor[i] != 0) empate = true;
    else if(presidente_vencedor[i] > M){
       M = presidente_vencedor[i];
       empate = false;
    }
  }
  if( M==0 ){
    cout << "Não há votos suficientes para realizar a eleição de presidente." << endl << endl;
  }
  else if(empate){
    cout << "Será realizado segundo turno entre ";
    for(i = 0; i < 100; i++){
      if(presidente_vencedor[i] == M){
        for(a = 0; a < size_candidatos; a++){
          if(stoi(newlista_de_candidatos[a].getNR_CANDIDATO()) == i && newlista_de_candidatos[a].getDS_CARGO() == "PRESIDENTE"){
            if(!empate) cout << ", ";
            empate = false;
            cout << newlista_de_candidatos[a].getNM_URNA_CANDIDATO();
            break;
          }
        }
      }
    }
    cout << endl << endl;
  }
  else {
    for(i = 0; i < size_candidatos; i++){
      if(presidente_vencedor[i] == M){
        cout << "O candidato eleito para o cargo de presidente foi ";
        for(a = 0; a < size_candidatos; a++){
          if(stoi(newlista_de_candidatos[a].getNR_CANDIDATO()) == i && newlista_de_candidatos[a].getDS_CARGO() == "PRESIDENTE"){
            cout << newlista_de_candidatos[a].getNM_URNA_CANDIDATO();
            break;
          }
        }
        break;
      }
    }
    cout << "." << endl << endl;
  }

  cout << "Governador:" << endl << endl;
  for(i = 0, M = 0; i < 100; i++) {
    if(governador_vencedor[i] == M && governador_vencedor[i] != 0) empate = true;
    else if(governador_vencedor[i] > M){
       M = governador_vencedor[i];
       empate = false;
    }
  }
  if( M==0 ){
    cout << "Não há votos suficientes para realizar a eleição de governador." << endl << endl;
  }
  else if(empate){
    cout << "Será realizado segundo turno entre ";
    for(i = 0; i < 100; i++){
      if(governador_vencedor[i] == M){
        for(a = 0; a < size_candidatos; a++){
          if(stoi(newlista_de_candidatos[a].getNR_CANDIDATO()) == i && newlista_de_candidatos[a].getDS_CARGO() == "GOVERNADOR"){
            if(!empate) cout << ", ";
            empate = false;
            cout << newlista_de_candidatos[a].getNM_URNA_CANDIDATO();
            break;
          }
        }
      }
      cout << endl << endl;
    }
  }
  else {
    for(i = 0; i < size_candidatos; i++){
      if(governador_vencedor[i] == M){
        cout << "O candidato eleito para o cargo de governador foi ";
        for(a = 0; a < size_candidatos; a++){
          if(stoi(newlista_de_candidatos[a].getNR_CANDIDATO()) == i && newlista_de_candidatos[a].getDS_CARGO() == "GOVERNADOR"){
            cout << newlista_de_candidatos[a].getNM_URNA_CANDIDATO();
            break;
          }
        }
        break;
      }
    }
    cout << "." << endl << endl;
  }

  cout << "Deputado federal:" << endl << endl;
  for(i = 0, M = 0; i < 10000; i++){
    if(deputado_federal_vencedor[i] > M){
      M = deputado_federal_vencedor[i];
    }
  }
  if( M==0 ){
    cout << "Não há votos suficientes para realizar a eleição deputado federal." << endl << endl;
  }
  else {
    cout << "O(s) candidato(s) eleito(s) para o cargo de deputado federal foi/foram: " << endl << endl;
    for(i = 0; i < 10000; i++){
      if(deputado_federal_vencedor[i] == M){
        for(a = 0; a < size_candidatos; a++){
          if(stoi(newlista_de_candidatos[a].getNR_CANDIDATO()) == i && newlista_de_candidatos[a].getDS_CARGO() == "DEPUTADO FEDERAL"){
            cout << newlista_de_candidatos[a].getNM_URNA_CANDIDATO() << endl;
            break;
          }
        }
      }
    }
    cout << endl;
  }

  cout << "Deputado distrital:" << endl << endl;
  for(i = 0, M = 0; i < 100000; i++){
    if(deputado_distrital_vencedor[i] > M){
      M = deputado_distrital_vencedor[i];
    }
  }
  if( M==0 ){
    cout << "Não há votos suficientes para realizar a eleição de deputado distrital." << endl << endl;
  }
  else{
    cout << "O(s) candidato(s) eleito(s) para o cargo de deputado distrital foi/foram: " << endl << endl;
    for(i = 0; i < 100000; i++){
      if(deputado_distrital_vencedor[i] == M){
        for(a = 0; a < size_candidatos; a++){
          if(stoi(newlista_de_candidatos[a].getNR_CANDIDATO()) == i && newlista_de_candidatos[a].getDS_CARGO() == "DEPUTADO DISTRITAL"){
            cout << newlista_de_candidatos[a].getNM_URNA_CANDIDATO() << endl;
            break;
          }
        }
      }
    }
    cout << endl;
  }

  cout << "Senador:" << endl << endl;
  cout << "Os candidatos eleitos para o cargo de senador foram: " << endl << endl;
  for(i = 0, M = 0; i < 1000; i++){
    if(senador_1_vencedor[i] > M){
      M = senador_1_vencedor[i];
    }
  }
  if( M==0 ){
    cout << "Não há votos suficientes para realizar a eleição de senador (1º vaga)." << endl;
  }
  else{
    for(i = 0; i < 1000; i++){
      if(senador_1_vencedor[i] == M){
        for(a = 0; a < size_candidatos; a++){
          if(stoi(newlista_de_candidatos[a].getNR_CANDIDATO()) == i && newlista_de_candidatos[a].getDS_CARGO() == "SENADOR"){
            cout << newlista_de_candidatos[a].getNM_URNA_CANDIDATO() << endl;
            break;
          }
        }
        break;
      }
    }
  }
  for(i = 0, M = 0; i < 1000; i++){
    if(senador_2_vencedor[i] > M){
      M = senador_2_vencedor[i];
    }
  }
  if( M==0 ){
    cout << "Não há votos suficientes para realizar a eleição de senador (2º vaga)." << endl << endl;
  }
  else{
    for(i = 0; i < 1000; i++){
      if(senador_2_vencedor[i] == M){
        for(a = 0; a < size_candidatos; a++){
          if(stoi(newlista_de_candidatos[a].getNR_CANDIDATO()) == i && newlista_de_candidatos[a].getDS_CARGO() == "SENADOR"){
            cout << newlista_de_candidatos[a].getNM_URNA_CANDIDATO() << endl;
            break;
          }
        }
        break;
      }
    }
    cout << endl;
  }

  cout << "Votos em branco: " << votos_branco << endl;
  cout << "Votos nulo: " << votos_nulo << endl;
}
