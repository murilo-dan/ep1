# Exercício de Programação 1

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps_2018_2/ep1/wikis/Descricao)

## Como usar o projeto

* Compile o projeto com o comando:

```sh
make
```

* Execute o projeto com o comando:

```sh
make run
```

## Funcionalidades do projeto

O projeto simula as funcionalidades de uma urna eletrônica, tais como:

* Apresentação das informações do candidato ao inserir o código correspondente.

* Opção de votar em branco.

* Opção de votar nulo.

* Opção de corrigir o voto no caso de erro de digitação.

Além disso, o projeto possui algumas funcionalidades/características que uma urna eletrônica não possui, tais como:

* Necessidade de declaração do número de eleitores.

* Relatório de votação, apresentando os eleitores e os votos de cada um.

* Resultado das eleições, apresentando os candidatos eleitos para cada cargo.

## Observações

O projeto apresenta ao usuário como proceder em cada etapa.

Apesar de o programa dizer que para votar nulo é necessário digitar "0", digitar qualquer código de candidato que não seja válido resulta em voto nulo (o eleitor ainda tem a opção de corrigir o voto), assim como acontece em uma urna eletrônica real.

Realizar algum comando inválido (digitar CONFIRMA, CORRIGE ou BRANCO de forma diferente do que é apresentado na tela) retornará o usuário à tela inicial de voto para o cargo que está sendo votado no momento.

## Sobre o relatório do resultado das eleições

O relatório do resultado das eleições funciona adequadamente para os cargos de presidente e governador. O programa apresenta o vencedor no caso de maioria de votos e indica que um segundo turno ocorrerá, caso empate.

Para os cargos de deputado federal e deputado distrital, a abordagem foi um pouco diferente do que ocorre na realidade, devido a inconsistência do número de deputados que são eleitos. Para o programa, diferente da realidade, serão considerados eleitos todos os deputados federais e distritais que empatarem com o maior número de votos (se todos os deputados receberem 15 votos, por exemplo, todos serão considerados eleitos).

Para o cargo de senador, o resultado também difere da realidade. No caso de empate, o senador mais idoso é eleito. Para o programa, no caso de empate, será eleito o primeiro senador que for encontrado no arquivo .csv. Foi necessário deixar assim por alguns problemas de compilação ao acrescentar a variável "idade" para leitura.

## Bugs e problemas

Um problema muito específico ocorreu com o candidato a senador de número 333 (Juiz Everardo Ribeiro). Aparentemente, cada senador deve ter apenas um 1º suplente e um 2º suplente. O candidato em questão, no entanto, possuía dois de cada atrelados ao seu número (inclusive um dos suplentes, Takane Kiyotsuka, era 1º e 2º suplente do candidato ao mesmo tempo). Isso causou algumas inconsistências na apresentação do candidato. Com um pouco de pesquisa, descobri que um dos supostos suplentes desistiu do cargo. Assim, tomei a liberdade de retirar a suplente repetida de um dos cargos, deixando um suplente para 1º e 2º.
Pode ser que esse mesmo problema ocorra com algum outro candidato, mas essa falha é causada pelo arquivo fornecido pelo TSE.

## Referências

https://stackoverflow.com/ e http://www.cplusplus.com/ para dúvidas menores e específicas.

http://www.tse.jus.br/eleicoes/eleicoes-2018/simulador-de-votacao-na-urna-eletronica para simular uma urna eletrônica e suas funcionalidades.
